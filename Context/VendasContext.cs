using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ApiVendas.Entities;

namespace ApiVendas.Context
{
    public class VendasContext : DbContext
    {
        //Passar a conexão com o Banco de Dados
        public VendasContext(DbContextOptions<VendasContext> options) : base(options)
        {

        }

        //Entidades -> Tabelas
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
    }
}