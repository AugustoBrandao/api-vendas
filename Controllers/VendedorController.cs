using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ApiVendas.Entities;
using ApiVendas.Context;

namespace ApiVendas.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly VendasContext _context;

        //Construtor que recebe Context
        public VendedorController(VendasContext context)
        {
            _context = context;
        }

        //[ENDPOINT] - Criar Vendedor
        [HttpPost]
        public IActionResult Create(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();
            return Ok(vendedor);
        }

        //[ENDPOINT] - Obter Vendedor por Nome
        [HttpGet("ObterPorNome")]
        public IActionResult ObterPorNome(string nome)
        {
            var vendedor = _context.Vendedores.Where(x => x.Nome.Contains(nome));
            return Ok(vendedor);
        }

        //[ENDPOINT] - Atualizar Vendedor
        [HttpPut("{id}")]
        public IActionResult AtualizarVendedor(int id, Vendedor vendedor)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if(vendedorBanco == null)
            {
                return NotFound();
            }

            vendedorBanco.Nome = vendedor.Nome;
            vendedorBanco.Cpf = vendedor.Cpf;
            vendedorBanco.Email = vendedor.Email;
            vendedorBanco.Telefone = vendedor.Telefone;

            _context.Vendedores.Update(vendedorBanco);
            _context.SaveChanges();

            return Ok(vendedorBanco);
        }

        //[ENDPOINT] - Deletar Vendedor
        [HttpDelete("{id}")]
        public IActionResult DeletarVendedor(int id)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if(vendedorBanco == null)
            {
                return NotFound();
            }

            _context.Vendedores.Remove(vendedorBanco);
            _context.SaveChanges();
            return NoContent();
        }

        
    }
}