using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ApiVendas.Entities;
using ApiVendas.Context;

namespace ApiVendas.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendasController : ControllerBase
    {
        //_context recebe os DbSets
        private readonly VendasContext _context;

        //Construtor que recebe Context
        public VendasController(VendasContext context)
        {
            _context = context;
        }

        //[ENDPOINT] - Criar Vendas
        [HttpPost]
        public IActionResult Create(Venda venda)
        {
            venda.Status = "aguardando pagamento";     
            _context.Add(venda);
            _context.SaveChanges();
            return Ok(venda);
        }
        
        //[ENDPOINT] - Obter Venda por Id
        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);

            if(venda == null)
            {
                return NotFound();
            }

            return Ok(venda);
        }

        //[ENDPOINT] - Atualizar Venda 
        [HttpPut("{id}")]
        public IActionResult AtualizarVenda(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if(vendaBanco == null)
                return NotFound();

            string vendaBd = vendaBanco.Status;
            string vendaBody= venda.Status.ToLower();

            if(vendaBd == "aguardando pagamento" &&(vendaBody == "pagamento aprovado" || vendaBody == "cancelada" || vendaBody == "cancelado"))
            {
                vendaBanco.Status = venda.Status;

            }else if(vendaBd == "pagamento aprovado" &&(vendaBody == "enviado para transportadora" || vendaBody == "cancelada"|| vendaBody == "cancelado"))
            {
                vendaBanco.Status = venda.Status;
                
            }else if(vendaBd == "enviado para transportadora" && vendaBody == "entregue")
            {
                vendaBanco.Status = venda.Status;

            }else if(vendaBd == "cancelada" || vendaBody == "cancelado")
            {
                vendaBanco.Status = "cancelada";
            }else{
                vendaBanco.Status = vendaBanco.Status;
            }

            vendaBanco.NomeVendedor = venda.NomeVendedor;
            vendaBanco.Data = venda.Data;
            vendaBanco.Item1 = venda.Item1;

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return Ok(vendaBanco);
        }

        //[ENDPOINT] - Deletar Venda
        [HttpDelete("{id}")]
        public IActionResult DeletarVenda(int id)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if(vendaBanco == null)
            {
                return NotFound();
            }

            _context.Vendas.Remove(vendaBanco);
            _context.SaveChanges();
            return NoContent();
        }
    }
}