using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiVendas.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public string NomeVendedor { get; set; }
        public DateTime Data { get; set; }
        public string Status { get; set; }
        public string Item1 { get; set; }
    }
}