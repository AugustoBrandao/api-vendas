# Projeto Payment API  

## Descrição 
O **Projeto Payment API** foi desenvolvido por Augusto Brandão Barbosa, utilizando a linguagem C# e explorando as tecnologias utilizadas no bootcamp da Pottencial com a Digital Innovation One.</br>
A **API** é basicamente um **CRUD** de vendedores e vendas que foi desenvolvida com o auxílio do **Entity Framework ORM** para facilitar a integração com o **SQL Server**.

## Conexão com Banco de Dados e Criação de Tabelas
Antes de inciar a aplicação, é necessário realizar a criação do banco de dados local e das tabelas executando o seguinte comando no terminal:

```
dotnet-ef database update
```


Verificar no SQL Server Management Studio se houve a criação da base de dados e das tabelas. O resultado ficará da seguinte forma:

<img src="Images/01.png" height="400px"></br>.

## Acesso ao Swagger
Basta digitar no terminal o seguinte comando que o Swagger já será aberto no navegador padrão:

```
dotnet watch run
```

<img src="Images/13.png" height="300px"></br>
</br></br>

# Evidências - Entidade Venda

- ## Criação da Venda
A venda será criada com o status diferente da atividade de forma proposital, para que o programa altere o status para "Aguardando Pagamento" automaticamente, independente do que o usuário digitar:

<img src="Images/02.png" height="300px"></br>
<img src="Images/03.png" height="300px"></br>
<img src="Images/04.png" height="300px"></br></br></br>

- ## Pesquisa da Venda Por ID
<img src="Images/05.png" height="400px"></br>

- ## Atualizar Venda - Caso de Erro
Vamos atualizar a venda colocando um status que está fora de ordem.</br>
Por exemplo, quando uma venda está com o status "Aguardando Pagamento", ela poderá ser alterada para "Pagamento Aprovado" ou "Cancelado", mas se for colocado algum status fora da sequência como por exemplo "Entregue", o status continuará como "Aguardando Pagamento".</br>

<img src="Images/06.png" height="400px"></br>
<img src="Images/07.png" height="400px"></br>

- ## Atualizar Venda - Caso de Sucesso
Vamos atualizar a venda colocando um status que está da ordem imposta pela regra de negócio.</br>
Por exemplo, quando uma venda está com o status "Aguardando Pagamento", ela poderá ser alterada para "Pagamento Aprovado" ou "Cancelado", iremos alterar para "Pagamento Aprovado".</br>

<img src="Images/08.png" height="400px"></br>
<img src="Images/09.png" height="400px"></br>
<img src="Images/10.png" height="300px"></br>

- ## Deletar Venda
<img src="Images/11.png" height="400px"></br>
<img src="Images/12.png" height="300px"></br></br>

# Evidências - Entidade Vendedor

- ## Criação do Vendedor
<img src="Images/14.png" height="300px"></br>
<img src="Images/15.png" height="400px"></br>
<img src="Images/16.png" height="300px"></br>

- ## Edição do Vendedor
<img src="Images/17.png" height="300px"></br>
<img src="Images/18.png" height="400px"></br>

- ## Pesquisa Vendedor por Nome
<img src="Images/19.png" height="500px"></br>

- ## Exclusão do Vendedor por Id
<img src="Images/20.png" height="400px"></br>
<img src="Images/21.png" height="300px"></br>
